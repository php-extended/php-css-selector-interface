<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssAttributeSelectorInterface interface file.
 * 
 * This interface represents a selector that selects a node with the given
 * attribute.
 * 
 * @author Anastaszor
 */
interface CssAttributeSelectorInterface extends CssAbstractSelectorInterface
{
	
	/**
	 * Gets the operator that is used for comparisons.
	 * 
	 * @return string
	 */
	public function getOperator() : string;
	
	/**
	 * Gets the value that should be selected for.
	 * 
	 * @return string
	 */
	public function getValue() : string;
	
}
