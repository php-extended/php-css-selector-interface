<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssCompositeSelectorInterface interface file.
 * 
 * This class represents 
 * 
 * @author Anastaszor
 */
interface CssCompositeSelectorInterface extends CssAbstractSelectorInterface
{
	
	/**
	 * Gets the operator that is used for comparisons.
	 * 
	 * @return string
	 */
	public function getOperator() : string;
	
	/**
	 * Gets the before node of this selector.
	 * 
	 * @return CssAbstractSelectorInterface
	 */
	public function getBeforeSelector() : CssAbstractSelectorInterface;
	
	/**
	 * Gets the after node of this selector.
	 * 
	 * @return CssAbstractSelectorInterface
	 */
	public function getAfterSelector() : CssAbstractSelectorInterface;
	
}
