<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use PhpExtended\Parser\ParserInterface;

/**
 * CssSelectorParserInterface interface file.
 * 
 * This class represents a parser for a css selector.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\ParserInterface<CssAbstractSelectorInterface>
 */
interface CssSelectorParserInterface extends ParserInterface
{
	
	// nothing to add
	// php does not accepts covariant return types between interfaces
	
}
