<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssElementSelectorInterface interface file.
 * 
 * This interface represents a selector of elements
 * 
 * @author Anastaszor
 */
interface CssElementSelectorInterface extends CssAbstractSelectorInterface
{
	
	/**
	 * Gets the namespace of the element selector.
	 * 
	 * @return string
	 */
	public function getNamespace() : string;
	
}
