<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

/**
 * CssStateSelectorInterface interface file.
 * 
 * This interface specifies a selector for a node with the given state.
 * 
 * @author Anastaszor
 */
interface CssStateSelectorInterface extends CssAbstractSelectorInterface
{
	
	/**
	 * Gets the wanted quantity of elements on this state.
	 * 
	 * @return int
	 */
	public function getQuantity() : int;
	
}
