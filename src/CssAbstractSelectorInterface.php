<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use Iterator;
use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlCollectionNodeInterface;
use SplStack;
use Stringable;

/**
 * CssAbstractSelectorInterface interface file.
 * 
 * This interface represents the root of a composite pattern.
 * 
 * @author Anastaszor
 * @abstract
 */
interface CssAbstractSelectorInterface extends Stringable
{
	
	/**
	 * Gets the name of the selector. The name of the selector is the value
	 * of the node in case of an element selector, the name of the attribute
	 * in case of an attribute selector, the name of the pseudo-class in case
	 * of state selector.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this selector is empty.
	 * 
	 * @return bool
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets whether this selector selects anythink of its kind.
	 * 
	 * @return bool
	 */
	public function isAny() : bool;
	
	/**
	 * Gets whether this node equals another node.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Makes this node be visited by the given visitor.
	 * 
	 * @param CssSelectorVisitorInterface $visitor
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function beVisitedBy(CssSelectorVisitorInterface $visitor);
	
	/**
	 * Gets whether the given node matches this selector, assuming the given
	 * parent stack for the given node. The stack should be given such that
	 * the nearest parent node will be available to top().
	 * 
	 * @param HtmlAbstractNodeInterface $node
	 * @param SplStack<HtmlCollectionNodeInterface> $parentStack
	 * @return boolean true if it matches, false if it does not
	 */
	public function matches(HtmlAbstractNodeInterface $node, ?SplStack $parentStack = null) : bool;
	
	/**
	 * Gets the first node in the given tree that matches this selector, null
	 * if none matches. The stack should be given such that the nearest parent
	 * node will be available to top().
	 * 
	 * @param HtmlAbstractNodeInterface $tree
	 * @param int $position the index of the element, 0 is the first element
	 * @param SplStack<HtmlCollectionNodeInterface> $parentStack
	 * @return ?HtmlAbstractNodeInterface
	 */
	public function findNode(HtmlAbstractNodeInterface $tree, int $position = 0, ?SplStack $parentStack = null) : ?HtmlAbstractNodeInterface;
	
	/**
	 * Gets all the nodes in the given tree that matches this selector, an
	 * empty collection is returned if none matches.The stack should be given
	 * such that the nearest parent node will be available to top().
	 * 
	 * @param HtmlAbstractNodeInterface $tree
	 * @param SplStack<HtmlCollectionNodeInterface> $parentStack
	 * @return Iterator<integer, HtmlAbstractNodeInterface>
	 */	
	public function findAllNodes(HtmlAbstractNodeInterface $tree, ?SplStack $parentStack = null) : Iterator;
	
}
