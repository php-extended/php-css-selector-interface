<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-css-selector-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Css;

use Stringable;

/**
 * CssSelectorVisitorInterface interface file.
 * 
 * This interface specifies an object that can walk the Css Selector Tree.
 * 
 * @author Anastaszor
 */
interface CssSelectorVisitorInterface extends Stringable
{
	
	/**
	 * Visits the given attribute selector.
	 * 
	 * @param CssAttributeSelectorInterface $selector
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitAttributeSelector(CssAttributeSelectorInterface $selector);
	
	/**
	 * Visits the given composite selector.
	 * 
	 * @param CssCompositeSelectorInterface $selector
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitCompositeSelector(CssCompositeSelectorInterface $selector);
	
	/**
	 * Visits the given element selector.
	 * 
	 * @param CssElementSelectorInterface $selector
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitElementSelector(CssElementSelectorInterface $selector);
	
	/**
	 * Visits the given state selector.
	 * 
	 * @param CssStateSelectorInterface $selector
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 */
	public function visitStateSelector(CssStateSelectorInterface $selector);
	
}
